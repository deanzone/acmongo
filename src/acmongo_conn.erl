%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. Apr 2018 9:04 PM
%%%-------------------------------------------------------------------
-module(acmongo_conn).
-author("adean").

%% API
-export([connect/1, disconnect/1]).

-export([ insert/3
        , update/4
        , update/6
        , delete/3
        , delete_one/3
        , delete_limit/4
%%        , insert/4
        , update/7
        , delete_limit/5
        ]).

-export([ find_one/3
        , find_one/4
        , find/3
        , find/4
%%        , find/2
%%        , find_one/2
        ]).

-export([ count/3
        , count/4
%%        , count/2
        ]).

%%-export([ command/2
%%        , command/3
%%       , sync_command/4
%%        , ensure_index/3
%%        , prepare/2
%%        ]).

-type cursor() :: pid().
-type collection() :: atom().
-type colldb() :: atom().
-type selector() :: map().

%%-type write_mode() :: unsafe | safe | {safe, bson:document()}.
%%-type read_mode() :: master | slave_ok.

-type args() :: [arg()].
-type arg() :: {database, atom()}
| {login, binary()}
| {password, binary()}
%%| {w_mode, write_mode()}
%%| {r_mode, read_mode()}
| {host, list()}
| {port, integer()}
| {register, atom() | fun()}
| {next_req_fun, fun()}.

%% @doc Make one connection to server, return its pid
-spec connect(args()) -> {ok, pid()}.
connect(Args) ->
  lager:info("Adding Mongo Connection"),
  mc_worker_api:connect(Args).

-spec disconnect(pid()) -> ok.
disconnect(Connection) ->
  lager:info("Removing Mongo Connection"),
  mc_worker_api:disconnect(Connection).


-spec insert(atom(), atom(), A) -> A.
insert(Pool, Colls, Docs) ->
  in_transaction(fun(Pid) -> mc_worker_api:insert(Pid, Colls, Docs) end, Pool).

-spec update(atom(), collection(), selector(), map()) -> {boolean(), map()}.
update(Pool, Coll, Selector, Doc) ->
  in_transaction(fun(Pid) -> mc_worker_api:update(Pid, Coll, Selector, Doc) end, Pool).

-spec update(atom(), collection(), selector(), map(), boolean(), boolean()) -> {boolean(), map()}.
update(Pool, Coll, Selector, Doc, Upsert, MultiUpdate) ->
  in_transaction(fun(Pid) -> mc_worker_api:update(Pid, Coll, Selector, Doc, Upsert, MultiUpdate) end, Pool).

-spec update(atom(), collection(), selector(), map(), boolean(), boolean(), map()) -> {boolean(), map()}.
update(Pool, Coll, Selector, Doc, Upsert, MultiUpdate, WC) ->
  BWC = to_bson_document(WC),
  in_transaction(fun(Pid) -> mc_worker_api:update(Pid, Coll, Selector, Doc, Upsert, MultiUpdate, BWC) end, Pool).

%% @doc Delete selected documents
-spec delete(pid(), collection(), selector()) -> {boolean(), map()}.
delete(Pool, Coll, Selector) ->
  delete_limit(Pool, Coll, Selector, 0).

%% @doc Delete first selected document.
-spec delete_one(pid(), collection(), selector()) -> {boolean(), map()}.
delete_one(Pool, Coll, Selector) ->
  delete_limit(Pool, Coll, Selector, 1).

%% @doc Delete selected documents
-spec delete_limit(pid(), collection(), selector(), integer()) -> {boolean(), map()}.
delete_limit(Pool, Coll, Selector, N) ->
  in_transaction(fun(Pid) -> mc_worker_api:delete_limit(Pid, Coll, Selector, N) end, Pool).

%% @doc Delete selected documents
-spec delete_limit(atom(), collection(), selector(), integer(), map()) -> {boolean(), map()}.
delete_limit(Pool, Coll, Selector, N, WC) ->
  BWC = to_bson_document(WC),
  in_transaction(fun(Pid) -> mc_worker_api:delete_limit(Pid, Coll, Selector, N, BWC) end, Pool).

%% @doc Return first selected document, if any
-spec find_one(pid(), colldb(), selector()) -> map() | undefined.
find_one(Pool, Coll, Selector) ->
  in_transaction(fun(Pid) -> mc_worker_api:find_one(Pid, Coll, Selector) end, Pool).

%% @doc Return first selected document, if any
-spec find_one(pid(), colldb(), selector(), map()) -> map() | undefined.
find_one(Pool, Coll, Selector, Args) ->
  in_transaction(fun(Pid) -> mc_worker_api:find_one(Pid, Coll, Selector, Args) end, Pool).

%% @doc Return first selected document, if any
%%-spec find_one(pid() | atom(), query()) -> map() | undefined.
%%find_one(Connection, Query) when is_record(Query, query) ->
%%  mc_connection_man:read_one(Connection, Query).

%% @doc Return selected documents.
-spec find(pid(), colldb(), selector()) -> {ok, cursor()} | [].
find(Connection, Coll, Selector) ->
  find(Connection, Coll, Selector, #{}).

%% @doc Return cursor to a projection of selected documents.
%%      Empty projection [] means full projection.
-spec find(pid(), colldb(), selector(), map()) -> {ok, pid()} | [].
find(Pool, Coll, Selector, Args) ->
  in_transaction(fun(Pid) -> mc_worker_api:find(Pid, Coll, Selector, Args) end, Pool).

%%-spec find(pid() | atom(), query()) -> {ok, cursor()} | [].
%%find(Connection, Query) when is_record(Query, query) ->

%% ============   Count =========================
%% @doc Count selected documents
-spec count(atom(), collection(), selector()) -> integer().
count(Pool, Coll, Selector) ->
  count(Pool, Coll, Selector, #{}).

%% @doc Count selected documents up to given max number; 0 means no max.
%%     Ie. stops counting when max is reached to save processing time.
-spec count(atom(), collection(), selector(), map()) -> integer().
count(Pool, Coll, Selector, Args) ->
  in_transaction(fun(Pid) -> mc_worker_api:count(Pid, Coll, Selector, Args) end, Pool).


%% ============= Utility ==========================
%%-spec prepare(tuple() | list() | map(), fun()) -> list().
%%prepare(Docs, AssignFun) when is_tuple(Docs) -> %bson
%%  mc_worker_api:prepare(Docs, AssignFun).

%% @doc Create index on collection according to given spec.
%%      The key specification is a bson documents with the following fields:
%%      IndexSpec      :: bson document, for e.g. {field, 1, other, -1, location, 2d}, <strong>required</strong>
%%-spec ensure_index(pid(), colldb(), bson:document()) -> ok | {error, any()}.
%%ensure_index(Connection, Coll, IndexSpec) ->
%%  mc_connection_man:request_worker(Connection, #ensure_index{collection = Coll, index_spec = IndexSpec}).
%%
%%%% @doc Execute given MongoDB command and return its result.
%%-spec command(pid(), mc_worker_api:selector()) -> {boolean(), map()} | {ok, cursor()}.
%%command(Connection, Query = #query{selector = Cmd}) -> % Action
%%
%%%% @doc Execute MongoDB command in this thread
%%-spec sync_command(port(), binary(), mc_worker_api:selector(), module()) -> {boolean(), map()}.
%%sync_command(Socket, Database, Command, SetOpts) ->


-spec in_transaction(Func :: function(), atom()) -> {true, _} | {error, []}.
in_transaction(Func, PoolName) ->
  lager:debug(">>>>>>> Trying to get a Pool Member from ~p", [PoolName]),
  case pooler:take_group_member(PoolName) of
    Pid when is_pid(Pid)->
      try Func(Pid) of
        Result ->
          pooler:return_group_member(PoolName, Pid, ok),
          Result
      catch
        Exception:Reason ->
          lager:error("in_transaction_catch: ~p - ~p", [Exception, Reason]),
          lager:error("~p", [erlang:get_stacktrace()]),
          pooler:return_group_member(PoolName, Pid, fail),
          {error, []}
      end;
    error_no_members  ->
      lager:error("Pooler Error: No Members for Pool ~p, increase max connections", [PoolName]),
      {error, []};
    {error_no_group, Pool} ->
      lager:error("Invalid Pool/Group ~p", [Pool]),
      {error, []}
  end.

-spec to_bson_document(map()) -> bson:document().
%%to_bson_document(List) when is_list(List) ->
%%  erlang:list_to_tuple(lists:reverse(lists:foldl(fun({K, V}, Acc) -> [K, V | Acc] end, [], List)));

to_bson_document(Map) when is_map(Map) ->
  erlang:list_to_tuple(lists:reverse(maps:fold(fun(K, V, Acc) -> [K, V | Acc] end, [], Map))).

