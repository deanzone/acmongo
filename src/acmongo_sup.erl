%%%-------------------------------------------------------------------
%% @doc acmongo top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(acmongo_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
  create_db_pools(),
  {ok, { {one_for_all, 0, 1}, []} }.

%%====================================================================
%% Internal functions
%
%====================================================================

get_mongo_config(CfgMap = #{database := _}) ->
  lists:foldl(fun(K, Acc) ->
    case maps:get(K, CfgMap, nil) of
      nil -> Acc;
      Val -> [{K, Val} | Acc]
    end
    end, [], [login, password, w_mode, r_mode, host, port, database]).


create_pool(CfgName, CfgMap = #{database := _}) ->
  MaxConn = maps:get(max_connections, CfgMap, 10),
  InitConn = maps:get(init_connections, CfgMap, max(5, ceil(MaxConn / 3))),
  MongoCfg = get_mongo_config(CfgMap),
  lager:info("Config ~p", [MongoCfg]),
  GroupName = maps:get(group, CfgMap, CfgName),
  PoolConfig = [
    {name, CfgName},
    {group, GroupName},
    {max_count, MaxConn},
    {init_count, InitConn},
    {start_mfa, {acmongo_conn, connect, [MongoCfg]}},
    {stop_mfa, {acmongo_conn, disconnect, ["$pooler_pid"]}}
  ],
  pooler:new_pool(PoolConfig);

create_pool(_CfgName, _CfgMap) -> ok.

create_db_pools() ->
  lager:info("Creating MongoDB Pools..."),
  Cfgs = application:get_all_env(acmongo),
  lists:foreach(fun({CfgName, Cfg}) -> create_pool(CfgName, maps:from_list(Cfg)) end, Cfgs).

